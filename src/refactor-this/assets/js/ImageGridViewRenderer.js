function ImageGridViewRenderer() {}

ImageGridViewRenderer.prototype.render = function () {
  
  document.getElementById("main-view").innerHTML = ImageDataGetter.navBar();

  var page = ImageDataGetter.getCurrentPage()
  var category = ImageDataGetter.getImageCategory();
  if (category) {
    document.getElementById("main-view").innerHTML +=
     '<div class="container">'
    +'  <div id="'+category+'-images" class="row row-cols-3"></div>'
    +'</div>'
    for(var x = 2; x >= 0; x--){
      ImageDataGetter.getImagesFromPage((page * 3) - x, category)
    }
  }
  
  document.getElementById("main-view").innerHTML += ImageDataGetter.pagination(page)
}
