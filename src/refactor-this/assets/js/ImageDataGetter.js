function ImageDataGetter() {}

// ImageDataGetter.getNatureImages = function () {
//   var images = []
//   return fetch('http://localhost:8888/images?category=nature')
//     .then(function (response) {
//       return response.json()
//     })
//     .then(function (result) {
//       images = result
//       return images
//     })
// }

ImageDataGetter.navBar = function () {
  var nav =
   '<nav class="navbar navbar-expand-lg navbar-light bg-light">'
  +'  <a class="navbar-brand" href="?nature">Photo Sharing App</a>'
  +'  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">'
  +'    <span class="navbar-toggler-icon"></span>'
  +'  </button>'
  +'  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">'
  +'    <div class="navbar-nav">'
  +'      <a class="nav-link active" href="?nature">Nature <span class="sr-only">"(current)"</span></a>'
  +'      <a class="nav-link" href="?architecture">Architecture</a>'
  +'      <a class="nav-link" href="?fashion">Fashion</a>'
  +'    </div>'
  +'  </div>'
  +'</nav>'

  return nav;
}

ImageDataGetter.getImageCategory = function (){
  var category = window.location.search.includes('?') && window.location.search.split('?')[1] ? window.location.search.split('?')[1] : 'nature';
  return category
}

ImageDataGetter.getCurrentPage = function (){
  var page = window.location.search.includes('page') && Number(window.location.search.split('page=')[1]) ? Number(window.location.search.split('page=')[1]) : 1;
  return page;
}

ImageDataGetter.prevBtn = function(page){
  var prevsearchstr = this.getImageCategory() == 'nature' && window.location.search.split('&page')[0] == '' ? window.location.search.split('&page')[0]+'?nature&page=' + (page > 1 ? page - 1 : page) : window.location.search.split('&page')[0]+'&page=' + (page > 1 ? page - 1 : page)
  return page <= 1 ? '<li class="page-item disabled"><a class="page-link" href="' + prevsearchstr + '">Previous</a></li>' : '<li class="page-item"><a class="page-link" href="' + prevsearchstr + '">Previous</a></li>'
  
}

ImageDataGetter.nextBtn = function(page){
  var nextsearchstr = this.getImageCategory() == 'nature' && window.location.search.split('&page')[0] == '' ? window.location.search.split('&page')[0]+'?nature&page=' + (page >= 1 ? page + 1 : page) : window.location.search.split('&page')[0]+'&page=' + (page >= 1 ? page + 1 : page)
  return page > 1 ? '<li class="page-item"><a class="page-link" href="' + nextsearchstr + '">Next</a></li>' : '<li class="page-item"><a class="page-link" href="' + nextsearchstr + '">Next</a></li>'
}

ImageDataGetter.pagination = function(page){
  var pagination =
   '<nav>'
  +'  <ul class="pagination ">'
  + this.prevBtn(page)
  + this.nextBtn(page)
  +'  </ul>'
  +'</nav>'

  return pagination;
}

ImageDataGetter.getImagesFromPage = function (page, category) {
  var images = []
  const fetchImages =  fetch('http://localhost:8888/images?category='+category.split('&')[0]+'&page=' + page);

  Promise.all([fetchImages]).then(values => {
    return Promise.all(values.map(r => r.json()))
  })
  .then((values) => {
    images = values[0]

    if(images.length > 0){
      for (var i = 0; i < images.length; i++) {
        document.getElementById(category+"-images").innerHTML +=
        '<div class="col" style="height: 400px; padding: 10px;">'
        +'  <img loading="lazy" height="100%" width="100%" class="image" src="' + images[i].url + '" alt="' + images[i].name + '" style="height: 100%; object-fit: cover; width: 100%;" />'
        +'  <div class="middle">'
        +'    <a class="btn btn-dark" href="' + images[i].url + '" download="' + images[i].name + '">DOWNLOAD</a>'
        +'  </div>'
        +'</div>'
      }
    }
  })
}
